package parser

import (
	"errors"
	"regexp"
	"strconv"
	"sync"
	"time"
)

type HTTPMethodType int

const (
	_                            = iota
	HTTPMethodGet HTTPMethodType = iota
	HTTPMethodHead
	HTTPMethodPost
	HTTPMethodPut
	HTTPMethodDelete
	HTTPMethodConnect
	HTTPMethodOptions
	HTTPMethodTrace
	HTTPMethodPatch
	HTTPMethodUnknown
)

var (
	ErrInvalidFormat = errors.New("invalid log entry format")
)

// Common Log Format regular expressions.
var (
	// Remote hostname (or IP number if DNS hostname is not available, or if
	// DNSLookup is Off.
	CommonLogRemoteHostExpr = `(` + HostExpr + `|-)`

	// RFC1413's remote logname of the user.
	// > <user-id> ::= <octet-string>
	// > <octet-string> ::= 1*512<octet-characters>
	// > <octet-characters> ::=
	// >   <any octet from  00 to 377 (octal) except for
	// >    ASCII NUL (000), CR (015) and LF (012)>
	// This binary is not well suited for text-based file format logging such as
	// Common Log Format. Rather use a general capturing regexp "anything until
	// the field separator" so that it matches anything emited by the logger for
	// this field, as it will most likely escape it using whatever method such as
	// base64, etc. Note that it also matches the undefined field "-".
	CommonLogUserIdRFC1413Expr = `((?:[[:^blank:]]{1,512}))`

	// The username as which the user has authenticated himself.
	// Simple general regexp matching anything until the field separator, as it is
	// not further specified.
	CommonLogAuthUserExpr = `([[:^blank:]]+)`

	// Date and time of the request.
	// This field is mandatory and cannot be undefined using "-".
	// Broad regular expression to match any localized dates.
	// The actual date parser will further validate the field when accessed.
	CommonLogDateFormat = `02/Jan/2006:15:04:05 -0700`
	CommonLogDateExpr   = `(?:\[((?:(?:[0-2][0-9])|(?:3[0-1]))/[A-Z][a-z]{2}/[0-9]{4}(?::[0-9]{2}){3} (?:\+|\-)[0-9]{4})\])`

	// The request line exactly as it came from the client.
	// This field is mandatory and cannot be undefined using "-".
	CommonLogHTTPRequestExpr = `(?:"((` + HTTPMethodExpr + `)[[:blank:]]+(` + HTTPRequestExpr + `)[[:blank:]]+HTTP/([0-9]\.[0-9]))")`

	// The HTTP status code returned to the client.
	// This field is mandatory and cannot be undefined using "-".
	CommonLogHTTPStatusCodeExpr = `([1-5][0-9]{2})`

	// The content-length of the document transferred.
	// This field is mandatory and cannot be undefined using "-".
	CommonLogHTTPResponseSizeExpr = `([0-9]+)`

	CommonLogFieldSeparatorExpr = `(?:[[:blank:]]+)`

	CommonLogExpr = CommonLogRemoteHostExpr +
		CommonLogFieldSeparatorExpr +
		CommonLogUserIdRFC1413Expr +
		CommonLogFieldSeparatorExpr +
		CommonLogAuthUserExpr +
		CommonLogFieldSeparatorExpr +
		CommonLogDateExpr +
		CommonLogFieldSeparatorExpr +
		CommonLogHTTPRequestExpr +
		CommonLogFieldSeparatorExpr +
		CommonLogHTTPStatusCodeExpr +
		CommonLogFieldSeparatorExpr +
		CommonLogHTTPResponseSizeExpr

	commonLogRegexp = regexp.MustCompile(CommonLogExpr)
)

// Log entry in common log [file] format.
// Thread-safe and lazy implementation of the parser, which means that every
// value will be parsed the latest possible, when getters are used. However, a
// quick validation is performed when created.
//
// String format: `remotehost rfc931 authuser [date] "request" status bytes`
// Spec: https://www.w3.org/Daemon/User/Config/Logging.html#common-logfile-format
type LazyCommonLogEntry struct {
	// Remote hostname (or IP number if DNS hostname is not available, or if
	// DNSLookup is Off.
	remoteHost string

	// The remote logname of the user as of rfc1413.
	userIdentifier string

	// The username as which the user has authenticated himself.
	authUser string

	// Date and time of the request.
	date time.Time

	// The request line exactly as it came from the client.
	request struct {
		method  HTTPMethodType
		uri     string
		version string
	}

	// The HTTP status code returned to the client.
	status uint16

	// Size of the object returned to the client, measured in bytes.
	bytes uint64

	// Helper struct to safely parse some fields.
	parserState struct {
		sourceString string
		date         struct {
			str  string
			lock sync.Mutex
		}
		httpMethod struct {
			str  string
			lock sync.Mutex
		}
		status struct {
			str  string
			lock sync.Mutex
		}
		bytes struct {
			str  string
			lock sync.Mutex
		}
	}
}

func NewLazyCommonLogEntryParser(str string) (*LazyCommonLogEntry, error) {
	matches := commonLogRegexp.FindStringSubmatch(str)
	if matches == nil {
		return nil, ErrInvalidFormat
	}

	e := &LazyCommonLogEntry{}
	// No need to further parse some of the following fields
	e.parserState.sourceString = matches[0]
	e.remoteHost = matches[1]
	e.userIdentifier = matches[2]
	e.authUser = matches[3]
	e.parserState.date.str = matches[4]
	e.parserState.httpMethod.str = matches[6]
	e.request.uri = matches[7]
	e.request.version = matches[8]
	e.parserState.status.str = matches[9]
	e.parserState.bytes.str = matches[10]

	return e, nil
}

func (e *LazyCommonLogEntry) String() string {
	return e.parserState.sourceString
}

func (e *LazyCommonLogEntry) RemoteHost() string {
	return e.remoteHost
}

func (e *LazyCommonLogEntry) UserIdentifier() string {
	return e.userIdentifier
}

func (e *LazyCommonLogEntry) AuthUser() string {
	return e.authUser
}

func (e *LazyCommonLogEntry) Date() (time.Time, error) {
	e.parserState.date.lock.Lock()
	defer e.parserState.date.lock.Unlock()
	if len(e.parserState.date.str) > 0 {
		date, err := time.Parse(CommonLogDateFormat, e.parserState.date.str)
		if err != nil {
			return time.Time{}, err
		}
		e.parserState.date.str = ""
		e.date = date
	}
	return e.date, nil
}

func (e *LazyCommonLogEntry) HTTPRequestMethod() HTTPMethodType {
	e.parserState.httpMethod.lock.Lock()
	defer e.parserState.httpMethod.lock.Unlock()
	if len(e.parserState.httpMethod.str) > 0 {
		e.request.method = ParseHTTPRequestMethod(e.parserState.httpMethod.str)
		e.parserState.httpMethod.str = ""
	}
	return e.request.method
}

func ParseHTTPRequestMethod(str string) HTTPMethodType {
	method := HTTPMethodUnknown
	switch str {
	case "GET":
		method = HTTPMethodGet
	case "HEAD":
		method = HTTPMethodHead
	case "POST":
		method = HTTPMethodPost
	case "PUT":
		method = HTTPMethodPut
	case "DELETE":
		method = HTTPMethodDelete
	case "CONNECT":
		method = HTTPMethodConnect
	case "OPTIONS":
		method = HTTPMethodOptions
	case "TRACE":
		method = HTTPMethodTrace
	case "PATCH":
		method = HTTPMethodPatch
	}
	return method
}

func (e *LazyCommonLogEntry) HTTPRequestURI() string {
	return e.request.uri
}

func (e *LazyCommonLogEntry) HTTPRequestVersion() string {
	return e.request.version
}

func (e *LazyCommonLogEntry) HTTPStatusCode() (uint16, error) {
	e.parserState.status.lock.Lock()
	defer e.parserState.status.lock.Unlock()
	if len(e.parserState.status.str) > 0 {
		status, err := strconv.ParseUint(e.parserState.status.str, 10, 16)
		if err != nil {
			return 0, err
		}
		e.parserState.status.str = ""
		e.status = uint16(status)
	}
	return e.status, nil
}

func (e *LazyCommonLogEntry) HTTPResponseSize() (uint64, error) {
	e.parserState.bytes.lock.Lock()
	defer e.parserState.bytes.lock.Unlock()
	if len(e.parserState.bytes.str) > 0 {
		bytes, err := strconv.ParseUint(e.parserState.bytes.str, 10, 16)
		if err != nil {
			return 0, err
		}
		e.parserState.bytes.str = ""
		e.bytes = uint64(bytes)
	}
	return e.bytes, nil
}
