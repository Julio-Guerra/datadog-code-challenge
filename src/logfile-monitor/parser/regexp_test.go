package parser_test

import (
	"logfile-monitor/parser"
	"logfile-monitor/test"
	"regexp"
	"testing"
)

type testSuite struct {
	name  string
	expr  string
	tests []testEntry
}

type testEntry struct {
	str      string
	expected []string
}

var suites = []testSuite{
	{
		name:  "IPv4",
		expr:  parser.IPv4Expr,
		tests: ipv4Tests,
	},

	{
		name:  "IPv6",
		expr:  parser.IPv6Expr,
		tests: ipv6Tests,
	},

	{
		name:  "Hostname",
		expr:  parser.HostnameExpr,
		tests: hostnameTests,
	},

	{
		name:  "Host",
		expr:  parser.HostExpr,
		tests: hostTests,
	},
}

var ipv4Tests = []testEntry{
	{
		str:      "192.168.2.1",
		expected: []string{"192.168.2.1"},
	},

	{
		str:      "255.255.255.255",
		expected: []string{"255.255.255.255"},
	},

	{
		str:      "86.91.13.112",
		expected: []string{"86.91.13.112"},
	},

	{
		str:      "0.0.0.0",
		expected: []string{"0.0.0.0"},
	},

	{
		str:      "1.2.3.4",
		expected: []string{"1.2.3.4"},
	},

	{
		str:      "255.255.255.255.255",
		expected: []string{"255.255.255.255"},
	},

	{
		str:      "256.255.255.255",
		expected: nil,
	},

	{
		str:      "255.256.255.255",
		expected: nil,
	},

	{
		str:      "255.255.256.255",
		expected: nil,
	},

	{
		str:      "255.255.255.256",
		expected: nil,
	},

	{
		str:      "255.255.255",
		expected: nil,
	},

	{
		str:      "",
		expected: nil,
	},
} // !IPv4

var ipv6Tests = []testEntry{

	{
		str:      "",
		expected: nil,
	},

	{
		str:      "1:2:3:4:5:6:7:8",
		expected: []string{"1:2:3:4:5:6:7:8"},
	},

	{
		str:      "1::",
		expected: []string{"1::"},
	},

	{
		str:      "1:2:3:4:5:6:7::",
		expected: []string{"1:2:3:4:5:6:7::"},
	},

	{
		str:      "1::8",
		expected: []string{"1::8"},
	},

	{
		str:      "1:2:3:4:5:6::8",
		expected: []string{"1:2:3:4:5:6::8"},
	},

	{
		str:      "1:2:3:4:5:6::8",
		expected: []string{"1:2:3:4:5:6::8"},
	},

	{
		str:      "1::7:8",
		expected: []string{"1::7:8"},
	},

	{
		str:      "1:2:3:4:5::7:8",
		expected: []string{"1:2:3:4:5::7:8"},
	},

	{
		str:      "1:2:3:4:5::8",
		expected: []string{"1:2:3:4:5::8"},
	},

	{
		str:      "1::6:7:8",
		expected: []string{"1::6:7:8"},
	},

	{
		str:      "1:2:3:4::6:7:8",
		expected: []string{"1:2:3:4::6:7:8"},
	},

	{
		str:      "1:2:3:4::8",
		expected: []string{"1:2:3:4::8"},
	},

	{
		str:      "1::5:6:7:8",
		expected: []string{"1::5:6:7:8"},
	},

	{
		str:      "1:2:3::5:6:7:8",
		expected: []string{"1:2:3::5:6:7:8"},
	},

	{
		str:      "1:2:3::8",
		expected: []string{"1:2:3::8"},
	},

	{
		str:      "1::4:5:6:7:8",
		expected: []string{"1::4:5:6:7:8"},
	},

	{
		str:      "1:2::4:5:6:7:8",
		expected: []string{"1:2::4:5:6:7:8"},
	},

	{
		str:      "1:2::8",
		expected: []string{"1:2::8"},
	},

	{
		str:      "1::3:4:5:6:7:8",
		expected: []string{"1::3:4:5:6:7:8"},
	},

	{
		str:      "1::3:4:5:6:7:8",
		expected: []string{"1::3:4:5:6:7:8"},
	},

	{
		str:      "1::8",
		expected: []string{"1::8"},
	},

	{
		str:      "::2:3:4:5:6:7:8",
		expected: []string{"::2:3:4:5:6:7:8"},
	},

	{
		str:      "::2:3:4:5:6:7:8",
		expected: []string{"::2:3:4:5:6:7:8"},
	},

	{
		str:      "::8",
		expected: []string{"::8"},
	},

	{
		str:      "::",
		expected: []string{"::"},
	},

	{
		str:      "fe80::7:8%eth0",
		expected: []string{"fe80::7:8%eth0"},
	},

	{
		str:      "fe80::7:8%1",
		expected: []string{"fe80::7:8%1"},
	},

	{
		str:      "::255.255.255.255",
		expected: []string{"::255.255.255.255"},
	},

	{
		str:      "::ffff:255.255.255.255",
		expected: []string{"::ffff:255.255.255.255"},
	},

	{
		str:      "::ffff:0:255.255.255.255",
		expected: []string{"::ffff:0:255.255.255.255"},
	},

	{
		str:      "2001:db8:3:4::192.0.2.33",
		expected: []string{"2001:db8:3:4::192.0.2.33"},
	},

	{
		str:      "64:ff9b::192.0.2.33",
		expected: []string{"64:ff9b::192.0.2.33"},
	},
} // !IPv6

var hostnameTests = []testEntry{
	{
		str:      "datadog.com",
		expected: []string{"datadog.com"},
	},

	{
		str:      "api.datadog.com",
		expected: []string{"api.datadog.com"},
	},

	{
		str:      "api.zone.datadog.com",
		expected: []string{"api.zone.datadog.com"},
	},

	{
		str:      "",
		expected: nil,
	},

	{
		str:      "datadog",
		expected: []string{"datadog"},
	},

	{
		str:      "mobile.mail.us-east-1.awsapps.com",
		expected: []string{"mobile.mail.us-east-1.awsapps.com"},
	},
} // !Hostname

var hostTests = append(ipv6Tests, append(ipv4Tests, hostnameTests...)...)

func TestRegularExpressions(t *testing.T) {
	testRegularExpressions(t, suites)
}

func testRegularExpressions(t *testing.T, suites []testSuite) {
	for _, suite := range suites {
		// Capture the loop variable in this scope.
		suite := suite
		t.Run(suite.name, func(t *testing.T) {
			re, err := regexp.Compile(suite.expr)
			if err != nil {
				test.Fail(t, nil, err)
			}
			re.Longest()

			for _, subtest := range suite.tests {
				subtest := subtest
				t.Run(subtest.str, func(t *testing.T) {
					matches := re.FindStringSubmatch(subtest.str)

					// Short workarount to permit reusing tests without and with one group
					// capturing.
					expected := subtest.expected
					if len(subtest.expected) == 1 && len(matches) == 2 {
						expected = append(expected, expected[0])
						// expected now looks like ["match", "match"], which corresponds to
						// submatches of "(pattern)".
					}

					if len(matches) != len(expected) {
						test.Fail(t, expected, matches)
					} else {
						for i := range matches {
							if matches[i] != expected[i] {
								test.Fail(t, expected, matches)
							}
						}
					}
				})
			}
		})
	}
}
