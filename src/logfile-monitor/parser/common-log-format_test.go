package parser_test

import (
	"fmt"
	"logfile-monitor/parser"
	"logfile-monitor/test"
	"testing"
	"time"
)

var commonLogSuites = []testSuite{
	{
		name:  "CommonLogRemoteHost",
		expr:  parser.CommonLogRemoteHostExpr,
		tests: append(commonLogRemoteHostTests, hostTests...),
	},

	{
		name:  "CommonLogUserIdRFC1413",
		expr:  parser.CommonLogUserIdRFC1413Expr,
		tests: append(commonLogUserIdRFC1413Tests, commonLogUserIdRFC1413OnlyTests...),
	},

	{
		name: "CommonLogAuthUserExpr",
		expr: parser.CommonLogAuthUserExpr,
		// Reusing UserId tests.
		tests: commonLogUserIdRFC1413Tests,
	},

	{
		name:  "CommonLogDateExpr",
		expr:  parser.CommonLogDateExpr,
		tests: commonLogDateTests,
	},

	{
		name:  "CommonLogHTTPRequestExpr",
		expr:  parser.CommonLogHTTPRequestExpr,
		tests: commonLogHTTPRequestTests,
	},

	{
		name:  "CommonLogHTTPStatusCodeExpr",
		expr:  parser.CommonLogHTTPStatusCodeExpr,
		tests: commonLogHTTPStatusCodeTests,
	},

	{
		name:  "CommonLogHTTPResponseSizeExpr",
		expr:  parser.CommonLogHTTPResponseSizeExpr,
		tests: commonLogHTTPResponseSizeTests,
	},

	{
		name:  "CommonLogExpr",
		expr:  parser.CommonLogExpr,
		tests: commonLogTests,
	},
}

var commonLogRemoteHostTests = []testEntry{
	{
		str:      "-",
		expected: []string{"-"},
	},
}

var commonLogUserIdRFC1413Tests = []testEntry{
	{
		str:      "-",
		expected: []string{"-"},
	},

	{
		str:      "myid ",
		expected: []string{"myid"},
	},

	{
		str:      id64,
		expected: []string{id64},
	},

	{
		str:      id128,
		expected: []string{id128},
	},

	{
		str:      id256,
		expected: []string{id256},
	},

	{
		str:      id512,
		expected: []string{id512},
	},
}

var commonLogUserIdRFC1413OnlyTests = []testEntry{
	{
		str:      id512 + "datadog",
		expected: []string{id512},
	},
}

var (
	// TODO: generate randomly.
	id64  = "a948904f2f0f479b8f8197694b30184b0d2ed1c1cd2a1ec0fb85d299a192a447"
	id128 = id64 + id64
	id256 = id128 + id128
	id512 = id256 + id256
)

var commonLogDateTests = []testEntry{
	{
		str:      "-",
		expected: nil,
	},

	{
		str:      "[09/May/2018:16:00:39]",
		expected: nil,
	},

	{
		str:      "[09/May/2018 16:00:39 +1200]",
		expected: nil,
	},

	{
		str:      "[09/May/2018:16:00:39 +1200]",
		expected: []string{"[09/May/2018:16:00:39 +1200]", "09/May/2018:16:00:39 +1200"},
	},
}

// TODO: do every possible combination with 3 arrays: one for the method, one
// for the request, one for the version.
var commonLogHTTPRequestTests = []testEntry{
	{
		str:      "-",
		expected: nil,
	},

	{
		str:      `"UPDATE / HTTP/1.1"`,
		expected: nil,
	},

	{
		str:      `"GET index.html HTTP/1.1"`,
		expected: nil,
	},

	{
		str:      `"GET /index.html HTTP/1"`,
		expected: nil,
	},

	{
		str:      `"/index.html HTTP/1.0"`,
		expected: nil,
	},

	{
		str:      `"GET / HTTP/1.1"`,
		expected: []string{`"GET / HTTP/1.1"`, "GET / HTTP/1.1", "GET", "/", "1.1"},
	},

	{
		str:      `"GET /one/two/three.js?one=!&two=three,four HTTP/1.1"`,
		expected: []string{`"GET /one/two/three.js?one=!&two=three,four HTTP/1.1"`, "GET /one/two/three.js?one=!&two=three,four HTTP/1.1", "GET", "/one/two/three.js?one=!&two=three,four", "1.1"},
	},

	{
		str:      `"GET /one/two/three.js?one=!&two=three,four HTTP/1.0"`,
		expected: []string{`"GET /one/two/three.js?one=!&two=three,four HTTP/1.0"`, "GET /one/two/three.js?one=!&two=three,four HTTP/1.0", "GET", "/one/two/three.js?one=!&two=three,four", "1.0"},
	},

	{
		str:      `"GET /one/two/three.js?one=!&two=three,four HTTP/2.0"`,
		expected: []string{`"GET /one/two/three.js?one=!&two=three,four HTTP/2.0"`, "GET /one/two/three.js?one=!&two=three,four HTTP/2.0", "GET", "/one/two/three.js?one=!&two=three,four", "2.0"},
	},

	{
		str:      `"PUT /one/two/three.js?one=!&two=three,four HTTP/2.0"`,
		expected: []string{`"PUT /one/two/three.js?one=!&two=three,four HTTP/2.0"`, "PUT /one/two/three.js?one=!&two=three,four HTTP/2.0", "PUT", "/one/two/three.js?one=!&two=three,four", "2.0"},
	},

	{
		str:      `"POST /one/two/three.js?one=!&two=three,four HTTP/2.0"`,
		expected: []string{`"POST /one/two/three.js?one=!&two=three,four HTTP/2.0"`, "POST /one/two/three.js?one=!&two=three,four HTTP/2.0", "POST", "/one/two/three.js?one=!&two=three,four", "2.0"},
	},

	{
		str:      `"PATCH /one/two/three.js?one=!&two=three,four HTTP/2.0"`,
		expected: []string{`"PATCH /one/two/three.js?one=!&two=three,four HTTP/2.0"`, "PATCH /one/two/three.js?one=!&two=three,four HTTP/2.0", "PATCH", "/one/two/three.js?one=!&two=three,four", "2.0"},
	},

	{
		str:      `"DELETE /one/two/three.js?one=!&two=three,four HTTP/2.0"`,
		expected: []string{`"DELETE /one/two/three.js?one=!&two=three,four HTTP/2.0"`, "DELETE /one/two/three.js?one=!&two=three,four HTTP/2.0", "DELETE", "/one/two/three.js?one=!&two=three,four", "2.0"},
	},
}

var commonLogHTTPStatusCodeTests = []testEntry{
	{
		str:      "-",
		expected: nil,
	},

	{
		str:      "ok",
		expected: nil,
	},

	{
		str:      "1",
		expected: nil,
	},

	{
		str:      "12",
		expected: nil,
	},

	{
		str:      "123",
		expected: []string{"123"},
	},

	{
		str:      "599",
		expected: []string{"599"},
	},

	{
		str:      "600",
		expected: nil,
	},
}

var commonLogHTTPResponseSizeTests = []testEntry{
	{
		str:      "-",
		expected: nil,
	},

	{
		str:      "",
		expected: nil,
	},

	{
		str:      "0",
		expected: []string{"0"},
	},

	{
		str:      "1",
		expected: []string{"1"},
	},

	{
		str:      "3610458549",
		expected: []string{"3610458549"},
	},
}

// TODO: automatically generate tests by reusing every previous subfield tests.
var commonLogTests = []testEntry{
	{
		str:      "",
		expected: nil,
	},

	{
		str:      `- - - [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326`,
		expected: []string{`- - - [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326`, "-", "-", "-", "10/Oct/2000:13:55:36 -0700", "GET /apache_pb.gif HTTP/1.0", "GET", "/apache_pb.gif", "1.0", "200", "2326"},
	},

	{
		str:      `datadog.com httplogger julio [11/Jul/2018:12:44:36 +0200] "GET /apache_pb.gif HTTP/1.0" 200 2326`,
		expected: []string{`datadog.com httplogger julio [11/Jul/2018:12:44:36 +0200] "GET /apache_pb.gif HTTP/1.0" 200 2326`, "datadog.com", "httplogger", "julio", "11/Jul/2018:12:44:36 +0200", "GET /apache_pb.gif HTTP/1.0", "GET", "/apache_pb.gif", "1.0", "200", "2326"},
	},

	// Missing remotehost field.
	{
		str:      `httplogger julio [11/Jul/2018:12:44:36 +0200] "GET /apache_pb.gif HTTP/1.0" 200 2326`,
		expected: nil,
	},

	// Missing response size field.
	{
		str:      `datadog.com httplogger julio [11/Jul/2018:12:44:36 +0200] "GET /apache_pb.gif HTTP/1.0" 200`,
		expected: nil,
	},

	// Invalid date format.
	{
		str:      `datadog.com httplogger julio [11/Jul/2018 12:44:36 +0200] "GET /apache_pb.gif HTTP/1.0" 200 33`,
		expected: nil,
	},

	// Invalid http method.
	{

		str:      `datadog.com httplogger julio [11/Jul/2018:12:44:36 +0200] "UPDATE /apache_pb.gif HTTP/1.0" 200 33`,
		expected: nil,
	},
}

func TestCommonLogRegularExpressions(t *testing.T) {
	testRegularExpressions(t, commonLogSuites)
}

func TestLazyCommonLogEntry(t *testing.T) {
	t.Run("Validation error", func(t *testing.T) {
		_, err := parser.NewLazyCommonLogEntryParser(`datadog.com httplogger julio [11/Jul/2018:12:44:36 +0200] "UPDATE /apache_pb.gif HTTP/1.0" 200 33`)
		if err == nil || err != parser.ErrInvalidFormat {
			test.Fail(t, parser.ErrInvalidFormat, err)
		}
	})

	t.Run("Validation error", func(t *testing.T) {
		_, err := parser.NewLazyCommonLogEntryParser(`httplogger julio [11/Jul/2018:12:44:36 +0200] "GET /apache_pb.gif HTTP/1.0" 200 33`)
		if err == nil || err != parser.ErrInvalidFormat {
			test.Fail(t, parser.ErrInvalidFormat, err)
		}
	})

	t.Run("Validation error", func(t *testing.T) {
		_, err := parser.NewLazyCommonLogEntryParser(`datadog.com httplogger julio [11/Jul/2018:12:44:36 +0200] "GET /apache_pb.gif HTTP/1.0" - 33`)
		if err == nil || err != parser.ErrInvalidFormat {
			test.Fail(t, parser.ErrInvalidFormat, err)
		}
	})

	for _, test := range commonLogParserTests {
		test := test
		t.Run("Valid", func(t *testing.T) {
			testLazyCommonLogEntry(t, test)
		})
	}
}

var commonLogParserTests = []commonLogParserTest{
	{
		RemoteHost:         `127.0.0.1`,
		UserId:             `-`,
		AuthUser:           `james`,
		Date:               `09/May/2018:16:00:39 +0000`,
		RequestMethod:      `GET`,
		RequestURI:         `/report`,
		RequestVersion:     `1.0`,
		ResponseStatusCode: 200,
		ResponseSize:       33,
	},

	{
		RemoteHost:         `v3.api.datadog.com`,
		UserId:             `34fdaae31`,
		AuthUser:           `-`,
		Date:               `13/Jun/2018:23:00:39 +1100`,
		RequestMethod:      `PUT`,
		RequestURI:         `/report`,
		RequestVersion:     `2.0`,
		ResponseStatusCode: 400,
		ResponseSize:       27,
	},

	{
		RemoteHost:         `v3.api.datadog.com`,
		UserId:             `34fdaae31`,
		AuthUser:           `-`,
		Date:               `13/Jun/2018:23:00:39 +0600`,
		RequestMethod:      `DELETE`,
		RequestURI:         `/report/33`,
		RequestVersion:     `2.0`,
		ResponseStatusCode: 230,
		ResponseSize:       18,
	},
}

type commonLogParserTest struct {
	RemoteHost         string
	UserId             string
	AuthUser           string
	Date               string
	RequestMethod      string
	RequestURI         string
	RequestVersion     string
	ResponseStatusCode uint16
	ResponseSize       uint64
}

// Format a string from given entry fields, parse it, and check parsed values
// are equal to original entry fields.
func testLazyCommonLogEntry(t *testing.T, entry commonLogParserTest) {
	str := fmt.Sprintf(`%s %s %s [%s] "%s %s HTTP/%s" %d %d`,
		entry.RemoteHost,
		entry.UserId,
		entry.AuthUser,
		entry.Date,
		entry.RequestMethod,
		entry.RequestURI,
		entry.RequestVersion,
		entry.ResponseStatusCode,
		entry.ResponseSize)
	log, err := parser.NewLazyCommonLogEntryParser(str)
	if err != nil {
		test.Fail(t, nil, err)
	}
	if matched := log.String(); matched != str {
		test.Fail(t, str, matched)
	}
	if remoteHost := log.RemoteHost(); remoteHost != entry.RemoteHost {
		test.Fail(t, entry.RemoteHost, remoteHost)
	}
	if userId := log.UserIdentifier(); userId != entry.UserId {
		test.Fail(t, entry.UserId, userId)
	}
	if authUser := log.AuthUser(); authUser != entry.AuthUser {
		test.Fail(t, entry.AuthUser, authUser)
	}
	if date, err := log.Date(); err != nil {
		test.Fail(t, nil, err)
	} else if entryDate, err := time.Parse(parser.CommonLogDateFormat, entry.Date); err != nil {
		t.Error("unexpected test error", err)
	} else if !date.Equal(entryDate) {
		test.Fail(t, entryDate, date)
	}
	if requestMethod := log.HTTPRequestMethod(); requestMethod != parser.ParseHTTPRequestMethod(entry.RequestMethod) {
		test.Fail(t, entry.RequestMethod, requestMethod)
	}
	if requestURI := log.HTTPRequestURI(); requestURI != entry.RequestURI {
		test.Fail(t, entry.RequestURI, requestURI)
	}
	if requestVersion := log.HTTPRequestVersion(); requestVersion != entry.RequestVersion {
		test.Fail(t, entry.RequestVersion, requestVersion)
	}
	if responseCode, err := log.HTTPStatusCode(); err != nil {
		test.Fail(t, nil, err)
	} else if responseCode != entry.ResponseStatusCode {
		test.Fail(t, entry.ResponseStatusCode, responseCode)
	}
	if responseSize, err := log.HTTPResponseSize(); err != nil {
		test.Fail(t, nil, err)
	} else if responseSize != entry.ResponseSize {
		test.Fail(t, entry.ResponseSize, responseSize)
	}
}
