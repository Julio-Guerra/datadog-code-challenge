package parser

// Helper tool: https://www.debuggex.com/

const (
	IPv4Expr = `(?:\b(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b)`

	// Taken from: https://stackoverflow.com/questions/53497/regular-expression-that-matches-valid-ipv6-addresses
	IPv6Expr = `(?:\b(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?::[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(?::[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?::0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\b)`

	// Regexp for a valid DNS hostname.
	// RFC 952 says that hostnames must not start with a digit or a hyphen, and
	// must not end with a hyphen. However, subsequent specification RFC 1123
	// permits hostnames to start with digits.
	// For now, we won't allow it, as it would match ip addresses, even wrong ones.
	HostnameExpr = `(?:\b[a-zA-Z](?:\.|-|[a-zA-Z0-9])+(?:\.+[a-z]{2,})?\b)`

	HostExpr = `(?:` + IPv6Expr + `|` + IPv4Expr + `|` + HostnameExpr + `)`

	HTTPMethodExpr = `(?:(?:GET)|(?:HEAD)|(?:POST)|(?:PUT)|(?:DELETE)|(?:CONNECT)|(?:OPTIONS)|(?:TRACE)|(?:PATCH))`

	HTTPRequestExpr = `/[[:graph:]]*`
)
