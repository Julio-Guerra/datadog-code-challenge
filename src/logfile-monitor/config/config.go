package config

import "time"

// Statically-defined program's configuration.
const (
	// Length of the channel's queue of log entries.
	QueueLengthLogEntries = 10

	// Length of the channel's queue of metrics.
	QueueLengthMetrics = 10

	// Maximum number of ignored log format validation errors before they become
	// fatal.
	MaxLogFormatValidationErrors = 10

	// Time interval to display the monitored metrics.
	MetricsDisplayInterval = 10 * time.Second

	// Time window within which metrics are monitored for alerting.
	AlertingTimeInterval = 2 * time.Minute

	// Alerting message templates.
	AlertingMessage          = "High traffic generated an alert - hits = %f, triggered at %s\n"
	AlertingRecoveredMessage = "High traffic alert recovered at %s\n"

	// Default value of the alert threshold option flag.
	DefaultAlertRPSThreshold = float64(10)

	// Default monitored log file option flag.
	DefaultLogfile = "/var/log/access.log"
)
