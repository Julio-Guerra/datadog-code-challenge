package test

import (
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"

	loggen "github.com/acroquest/apache-loggen-go"
)

type LogFileMockup loggen.Config

func NewLogFileGenerator() (*LogFileMockup, error) {
	return (*LogFileMockup)(&loggen.Config{
		Format:  "%h %l %u %t \"%r\" %>s %b",
		Prefix:  "192.168.0.0/16",
		ErrRate: 0.01,
		Bytes: loggen.LogNormal{
			Mu:    0.0,
			Sigma: 0.5,
			Value: 2000,
		},
		ResponseTime: loggen.LogNormal{
			Mu:    0.0,
			Sigma: 0.5,
			Value: 20000,
		},
		Categories: []string{
			"api",
			"books",
			"electronics",
			"software",
			"games",
			"office",
			"cameras",
			"computers",
			"finance",
			"giftcards",
			"garden",
			"health",
			"music",
			"sports",
			"toys",
			"networking",
			"jewelry",
		},
	}), nil
}

func (l *LogFileMockup) Create() error {
	var file *os.File
	for {
		//  Safe implementation of exclusive file creation to avoid pseudo-random
		//  filename conflicts by ensuring the file doesn't exist yet.
		filename := fmt.Sprintf("/var/log/access-%d.log", rand.Intn(100000))
		var err error
		file, err = os.OpenFile(filename, os.O_CREATE|os.O_EXCL|os.O_RDWR, 0666)
		if err == nil {
			break
		}
		file.Close()
	}
	l.File = file
	return nil
}

func (l *LogFileMockup) GenerateNewRecord(t time.Time) string {
	return loggen.GenerateNewRecord((*loggen.Config)(l), t)
}

// Rotate the logfile.
// Safe implementation for concurrent calls to LogFileMockup.Create(). Simple
// implementation, good enough to see how the tail module reacts.
func (l *LogFileMockup) Rotate() (err error) {
	copyFile(l.File, l.File.Name()+".2")
	l.File.Seek(0, 0)
	l.File.Truncate(0)
	return nil
}

func copyFile(src *os.File, dst string) (err error) {
	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer out.Close()
	if _, err = io.Copy(out, src); err != nil {
		return
	}
	return
}
