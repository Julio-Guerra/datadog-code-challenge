package test

import "testing"

func Fail(t *testing.T, expected interface{}, got interface{}) {
	t.Logf("\nexpected:\n\t`%s`\ngot:\n\t`%s`", expected, got)
	t.Fail()
}
