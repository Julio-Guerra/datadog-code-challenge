package source

import (
	"context"
	"fmt"
	"log"
	"logfile-monitor/config"
	"logfile-monitor/parser"

	"github.com/hpcloud/tail"
)

// Create a log source from a regular log file.
// In such file, a log entry is a line represented.
func FromLogFile(ctx context.Context, logFile string) (<-chan *parser.LazyCommonLogEntry, error) {
	if logFile == "" {
		return nil, ErrEmptyFileName
	}

	t, err := tail.TailFile(logFile, tail.Config{
		ReOpen: true,
		Follow: true,
	})
	if err != nil {
		return nil, err
	}

	entries := make(chan *parser.LazyCommonLogEntry, config.QueueLengthLogEntries)
	go func() {
		// Signal no more entries when returning
		defer func() {
			fmt.Println("closing channel of log entries")
			close(entries)
			t.Stop()
		}()

		nbValidationErrors := 0
		for {
			select {
			case <-ctx.Done():
				// Context is done so return.

				if err := ctx.Err(); err != context.Canceled {
					log.Println("logfile: context is done with error", err)
				}
				return

			case line, ok := <-t.Lines:
				// New line from file or closed channel of lines.

				if !ok {
					log.Println("logfile: tail channel closed")
					return
				}

				log.Printf("logfile: new log entry: `%s`\n", line.Text)

				entry, err := parser.NewLazyCommonLogEntryParser(line.Text)
				if err != nil {
					if err != parser.ErrInvalidFormat {
						log.Println("logfile: parse error", err)
						return
					}

					log.Println("logfile: log format validation error", err)
					// Parsing errors are tolerated up to a certain amount.
					nbValidationErrors++
					if nbValidationErrors == config.MaxLogFormatValidationErrors {
						log.Println("logfile: too much log entries validation errors")
						return
					}
					continue
				}
				entries <- entry
			}
		}
	}()

	// Note: when necessary to manage errors such as the one above (which are here
	// simply logged), I use another channel only for errors so that the caller
	// can get the details and handle the actual error. And when there are no
	// errors, the channel is simply closed so that the caller gets nil.

	return entries, nil
}
