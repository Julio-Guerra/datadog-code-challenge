package source_test

import (
	"context"
	"logfile-monitor/parser"
	"logfile-monitor/source"
	"logfile-monitor/test"
	"math/rand"
	"testing"
	"time"
)

func TestFromLogFile(t *testing.T) {
	t.Run("Get log entries from an existing logfile", func(t *testing.T) {
		logfile, err := test.NewLogFileGenerator()
		if err != nil {
			t.Error(err)
		}
		if err = logfile.Create(); err != nil {
			t.Error(err)
		}
		t.Log("log file is", logfile.File.Name())

		ctx, cancel := context.WithCancel(context.Background())
		logs, err := source.FromLogFile(ctx, logfile.File.Name())
		if err != nil {
			t.Error(err)
		}
		testFromLogFile(t, logfile, logs, 25)
		cancel()
	})

	t.Run("Get log entries from a logrotated file", func(t *testing.T) {
		logfile, err := test.NewLogFileGenerator()
		if err != nil {
			t.Error(err)
		}
		if err = logfile.Create(); err != nil {
			t.Error(err)
		}
		t.Log("log file is", logfile.File.Name())

		ctx, cancel := context.WithCancel(context.Background())
		logs, err := source.FromLogFile(ctx, logfile.File.Name())
		if err != nil {
			t.Error(err)
		}
		_ = logs
		testFromLogFile(t, logfile, logs, 10)
		logfile.Rotate()
		testFromLogFile(t, logfile, logs, 5)

		cancel()
	})

	t.Run("Canceling the context leads to closing the channel", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		logs, err := source.FromLogFile(ctx, "I don't exist")
		if err != nil {
			t.Error(err)
		}
		// The fact that the file doesn't exist ensures no lines are sent in the
		// channel.
		cancel()
		_, ok := <-logs
		if ok {
			t.Fail()
		}
	})

	t.Run("Empty filename leads to error", func(t *testing.T) {
		_, err := source.FromLogFile(context.Background(), "")
		if err == nil || err != source.ErrEmptyFileName {
			t.Fail()
		}
	})
}

func testFromLogFile(t *testing.T, logfile *test.LogFileMockup, logs <-chan *parser.LazyCommonLogEntry, count int) {
	now := time.Now()
	for i := 0; i < count; i++ {
		record := logfile.GenerateNewRecord(now)
		log := <-logs
		if str := log.String(); str != record {
			test.Fail(t, record, str)
		}
		now = now.Add(time.Duration(1+rand.Intn(500)) * time.Millisecond)
	}
}
