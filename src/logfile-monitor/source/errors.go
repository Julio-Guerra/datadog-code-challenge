package source

import "errors"

var (
	ErrEmptyFileName = errors.New("empty filename")
)
