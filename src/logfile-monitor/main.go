package main

import (
	"context"
	"flag"
	"io"
	"log"
	"logfile-monitor/config"
	"logfile-monitor/pipeline"
	"os"
	"os/signal"
	"runtime"
)

func main() {
	// Correctly stop the execution when receiving an interrupt signal
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		defer signal.Stop(c)

		<-c
		log.Println("interrupt signal received")

		// cancel the context so that the program stops
		log.Println("canceling the program")
		cancel()
		// Yield to enforce scheduling every other goroutines before leaving the
		// signal handler.
		runtime.Gosched()
	}()

	optLogFile := flag.String(
		"logfile",
		config.DefaultLogfile,
		"Log file to monitor.")

	optAlertRPSThreshold := flag.Float64(
		"alert-rps-threshold",
		config.DefaultAlertRPSThreshold,
		"Request per second threshold of the alerting system.")

	flag.Parse()

	<-Pipeline(
		ctx,
		os.Stdout,
		*optLogFile,
		*optAlertRPSThreshold)
	// Cancel the context to stop still running stages. This case happens when an
	// error occurs in some stage. It thus dispatches its termination by canceling
	// its channel but then fixme: on tombe sur le cas ou le channel est plein et
	// la goroutine est bloquée sur l'ecriture de sa sortie. il faudrait donc
	// eviter ca en trouvant un moyen surement via select de check ctx en meme
	// temps.
	cancel()
}

func Pipeline(ctx context.Context, output io.Writer, logfile string, rpsAlertThreshold float64) <-chan struct{} {
	// Pipeline: Source -> Metrics -> Alerts
	logs := pipeline.SourceStage(ctx, logfile)
	metrics := pipeline.MetricsStage(ctx, logs, output, config.MetricsDisplayInterval)
	done := pipeline.AlertStage(metrics, config.AlertingTimeInterval, rpsAlertThreshold, output)
	return done
}
