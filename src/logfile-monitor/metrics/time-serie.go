package metrics

import (
	"time"
)

// Simple Time Serie, only storing time values.
type TimeSeries struct {
	plots       []time.Time
	maxDuration time.Duration
}

func NewTimeSeries(maxDuration time.Duration) *TimeSeries {
	return &TimeSeries{
		maxDuration: maxDuration,
	}
}

func (s *TimeSeries) Add(date time.Time) {
	s.plots = append(s.plots, date)
	// TODO: insert/sort by time

	if len(s.plots) <= 1 {
		return
	}

	last := len(s.plots) - 1
	var i int
	for i = 0; i < last && s.plots[last].Sub(s.plots[i]) > s.maxDuration; i++ {
	}

	if i < last && s.plots[last].Sub(s.plots[i]) > s.maxDuration {
		s.plots = s.plots[i : last+1]
	}
}

// Overall duration of the time serie.
func (s *TimeSeries) Duration() time.Duration {
	if len(s.plots) <= 1 {
		return 0
	}
	return s.plots[len(s.plots)-1].Sub(s.plots[0])
}

// Get the number of plots during the past maxDuration and return the duration
// between the first and the last one.
func (s *TimeSeries) PlotsDuringLast(maxDuration time.Duration) (int, time.Duration) {
	if len(s.plots) < 2 {
		return 0, 0
	}

	last := len(s.plots) - 1

	now := time.Now()
	if d := now.Sub(s.plots[last]); d > maxDuration {
		return 0, d
	}

	i := last - 1
	for ; i >= 0 && s.plots[last].Sub(s.plots[i]) <= maxDuration; i-- {
	}

	if i < 0 {
		d := s.plots[last].Sub(s.plots[0])
		if d <= maxDuration {
			return len(s.plots), d
		}
		return len(s.plots) - 1, d
	}

	i++
	return len(s.plots) - i - 1, s.plots[last].Sub(s.plots[i])
}
