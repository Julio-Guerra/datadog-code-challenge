package metrics

import (
	"logfile-monitor/test"
	"testing"
	"time"
)

func TestTimeSerie(t *testing.T) {
	t.Run("1m1s", func(t *testing.T) {
		s := NewTimeSeries(time.Minute)
		now := time.Now()
		s.Add(now)
		s.Add(now.Add(time.Second))
		s.Add(now.Add(time.Minute + time.Second))

		plots, duration := s.PlotsDuringLast(time.Minute)

		if expected := 1; expected != plots {
			test.Fail(t, expected, plots)
		}

		if expected := time.Minute; expected != duration {
			test.Fail(t, expected, duration)
		}
	})

	t.Run("1m", func(t *testing.T) {
		s := NewTimeSeries(time.Minute)
		n := time.Now()
		s.Add(n)
		n = n.Add(time.Second)
		s.Add(n)
		n = n.Add(time.Minute)
		s.Add(n)

		plots, duration := s.PlotsDuringLast(time.Minute)

		if expected := 1; expected != plots {
			test.Fail(t, expected, plots)
		}

		if expected := time.Minute; expected != duration {
			test.Fail(t, expected, duration)
		}
	})

	t.Run("1m1s", func(t *testing.T) {
		s := NewTimeSeries(time.Minute + time.Second)
		n := time.Now()
		s.Add(n)
		n = n.Add(time.Minute)
		s.Add(n)
		n = n.Add(time.Second)
		s.Add(n)
		n = n.Add(time.Minute)
		s.Add(n)

		plots, duration := s.PlotsDuringLast(time.Minute + time.Second)

		if expected := time.Minute + time.Second; expected < duration {
			test.Fail(t, expected, duration)
		}

		if expected := 2; expected != plots {
			test.Fail(t, expected, plots)
		}

	})

	t.Run("30s", func(t *testing.T) {
		s := NewTimeSeries(30 * time.Second)
		n := time.Now()
		n = n.Add(time.Minute)
		s.Add(n)
		n = n.Add(time.Minute)
		s.Add(n)
		n = n.Add(time.Minute)
		s.Add(n)

		plots, duration := s.PlotsDuringLast(30 * time.Second)

		if expected := 0; expected != plots {
			test.Fail(t, expected, plots)
		}

		if expected := time.Duration(0); expected != duration {
			test.Fail(t, expected, duration)
		}
	})

	t.Run("3m", func(t *testing.T) {
		s := NewTimeSeries(3 * time.Minute)
		n := time.Now()
		n = n.Add(time.Minute)
		s.Add(n)
		n = n.Add(time.Minute)
		s.Add(n)
		n = n.Add(time.Minute)
		s.Add(n)

		plots, duration := s.PlotsDuringLast(3 * time.Minute)

		if expected := 3; expected != plots {
			test.Fail(t, expected, plots)
		}

		if expected := 2 * time.Minute; expected != duration {
			test.Fail(t, expected, duration)
		}
	})

	t.Run("-10m", func(t *testing.T) {
		s := NewTimeSeries(3 * time.Minute)
		n := time.Now().Add(-10 * time.Minute)
		n = n.Add(time.Minute)
		s.Add(n)
		n = n.Add(time.Minute)
		s.Add(n)
		n = n.Add(time.Minute)
		s.Add(n)

		plots, duration := s.PlotsDuringLast(3 * time.Minute)

		if expected := 0; expected != plots {
			test.Fail(t, expected, plots)
		}

		// It's been 7 minutes without new events
		if expected := 7 * time.Minute; expected.Minutes() == duration.Minutes() {
			test.Fail(t, expected, duration)
		}
	})

}
