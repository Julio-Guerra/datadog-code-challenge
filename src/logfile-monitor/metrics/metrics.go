package metrics

import (
	"fmt"
	"io"
	"logfile-monitor/parser"
	"strings"
	"text/tabwriter"
	"time"
)

// Simple HTTP metrics structure storing some statistics update for each new
// log entry.
type HTTPMetrics struct {
	// Total duration of the metrics.
	duration time.Duration
	// Last log date added to the metrics.
	lastDate time.Time

	// Total number of requests.
	requests uint64
	// Average number of requests per second.
	requestsPerSecond float64

	// Total number of errors.
	errors uint64
	// Average percentage of request errors.
	errorRate float64

	// Total response size in bytes.
	bytes uint64
	// Average bytes (response size) per request.
	bytesPerRequest float64
	// Average bytes (response size) per second.
	bytesPerSecond float64

	// Map of accessed sections in descending order.
	hits map[string]int
}

func (m *HTTPMetrics) Update(log *parser.LazyCommonLogEntry) error {
	logDate, err := log.Date()
	if err != nil {
		return err
	}
	m.updateDuration(logDate)

	m.updateRequests()

	code, err := log.HTTPStatusCode()
	if err != nil {
		return err
	}
	m.updateErrors(code)

	bytes, err := log.HTTPResponseSize()
	if err != nil {
		return err
	}
	m.updateBytes(bytes)

	m.updateHits(log.HTTPRequestURI())

	return nil
}

func (m *HTTPMetrics) updateDuration(date time.Time) {
	if !m.lastDate.IsZero() {
		m.duration += date.Sub(m.lastDate)
	}
	m.lastDate = date
}

func (m *HTTPMetrics) updateRequests() {
	m.requests++
	if d := m.duration.Seconds(); d != 0 {
		m.requestsPerSecond = float64(m.requests) / d
	}
}

func (m *HTTPMetrics) updateErrors(code uint16) {
	if code >= 400 {
		m.errors++
	}
	if r := m.requests; r != 0 {
		m.errorRate = 100 * float64(m.errors) / float64(r)
	}
}

func (m *HTTPMetrics) updateBytes(bytes uint64) {
	m.bytes += bytes
	if r := m.requests; r != 0 {
		m.bytesPerRequest = float64(m.bytes) / float64(r)
	}
	if d := m.duration.Seconds(); d != 0 {
		m.bytesPerSecond = float64(m.bytes) / d
	}
}

func (m *HTTPMetrics) updateHits(uri string) {
	nodes := strings.Split(uri, "/")
	section := "/" + nodes[1] // asserted by log's validation.
	if m.hits == nil {
		m.hits = make(map[string]int)
	}
	m.hits[section] += 1
}

// Total duration of the metrics.
func (m *HTTPMetrics) Duration() time.Duration {
	return m.duration
}

// Last log date added to the metrics.
func (m *HTTPMetrics) LastDate() time.Time {
	return m.lastDate
}

// Total number of requests.
func (m *HTTPMetrics) Requests() uint64 {
	return m.requests
}

// Average number of requests per second.
func (m *HTTPMetrics) RequestsPerSecond() float64 {
	return m.requestsPerSecond
}

// Total number of errors.
func (m *HTTPMetrics) Errors() uint64 {
	return m.errors
}

// Average percentage of request errors.
func (m *HTTPMetrics) ErrorRate() float64 {
	return m.errorRate
}

// Total response size in bytes.
func (m *HTTPMetrics) Bytes() uint64 {
	return m.bytes
}

// Average bytes (response size) per request.
func (m *HTTPMetrics) BytesPerRequest() float64 {
	return m.bytesPerRequest
}

// Average bytes (response size) per second.
func (m *HTTPMetrics) BytesPerSecond() float64 {
	return m.bytesPerSecond
}

// Map of accessed sections in descending order.
func (m *HTTPMetrics) Hits() map[string]int {
	return m.hits
}

func PrintMetricsSummary(metrics *HTTPMetrics, output io.Writer) {
	fmt.Fprintln(output, "=== Summary Statistics ===")
	w := tabwriter.NewWriter(output, 20, 1, 3, ' ', 0)

	fmt.Fprintln(w, "METRIC\tVALUE")
	fmt.Fprintf(w, "Requests\t%d\n", metrics.Requests())
	fmt.Fprintf(w, "Requests/Second\t%.2f\n", metrics.RequestsPerSecond())
	fmt.Fprintf(w, "Errors\t%d\n", metrics.Errors())
	fmt.Fprintf(w, "Error Rate\t%.2f\n", metrics.ErrorRate())
	fmt.Fprintf(w, "Bytes/Request\t%.2f\n", metrics.BytesPerRequest())
	fmt.Fprintf(w, "Bytes/Second\t%.2f\n", metrics.BytesPerSecond())

	fmt.Fprintf(w, "\n")
	fmt.Fprintln(w, "SECTION\tHITS")
	n := 0
	for key, value := range metrics.Hits() {
		fmt.Fprintf(w, "%s\t%d\n", key, value)
		n++
	}
	if n == 0 {
		fmt.Fprintln(w, "No data yet monitored.")
	}

	w.Flush()
}
