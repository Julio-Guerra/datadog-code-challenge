package pipeline

import (
	"fmt"
	"io"
	"logfile-monitor/config"
	"logfile-monitor/metrics"
	"time"
)

func AlertStage(metricsChan <-chan metrics.HTTPMetrics, interval time.Duration, threshold float64, writer io.Writer) (done <-chan struct{}) {
	var ts metrics.TimeSeries
	alert := false
	d := make(chan struct{})
	go func() {
		defer close(d)
		for metric := range metricsChan {
			// Add the value to the time serie
			ts.Add(metric.LastDate())
			requests, duration := ts.PlotsDuringLast(interval)
			if requests == 0 {
				// Not enough values for now.
				continue
			}

			if duration.Seconds() == 0 {
				duration = time.Second
			}

			var rps float64
			if rps = float64(requests) / duration.Seconds(); !alert && rps >= threshold {
				fmt.Fprintf(writer, config.AlertingMessage, rps, time.Now())
				alert = true
			} else if alert && rps < threshold {
				fmt.Fprintf(writer, config.AlertingRecoveredMessage, time.Now())
				alert = false
			}
		}
	}()
	return d
}
