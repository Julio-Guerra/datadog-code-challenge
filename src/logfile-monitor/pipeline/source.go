package pipeline

import (
	"context"
	"log"
	"logfile-monitor/parser"
	"logfile-monitor/source"
)

// The source stage reads lines from the log file and sends them to the next
// stage.
func SourceStage(ctx context.Context, logfile string) <-chan *parser.LazyCommonLogEntry {
	entries, err := source.FromLogFile(ctx, logfile)
	if err != nil {
		log.Fatal(err)
	}
	return entries
}
