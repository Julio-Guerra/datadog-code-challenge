package pipeline

import (
	"context"
	"io"
	"log"
	"logfile-monitor/config"
	"logfile-monitor/metrics"
	"logfile-monitor/parser"
	"sync"
	"time"
)

func MetricsStage(ctx context.Context, logs <-chan *parser.LazyCommonLogEntry, summaryOutput io.Writer, summaryInterval time.Duration) <-chan metrics.HTTPMetrics {
	var (
		stats metrics.HTTPMetrics
		// Lock for the concurrent access to stats from the symmary display
		// goroutine and the pipeline stage.
		lock sync.RWMutex
	)

	// Automatic metrics display every summaryInterval.
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(summaryInterval):
				lock.RLock()
				metrics.PrintMetricsSummary(&stats, summaryOutput)
				lock.RUnlock()
			}
		}
	}()

	metrics := make(chan metrics.HTTPMetrics, config.QueueLengthMetrics)
	go func() {
		defer close(metrics)
		for entry := range logs {
			lock.Lock()
			if err := stats.Update(entry); err != nil {
				lock.Unlock()
				log.Println(err)
				return
			}
			lock.Unlock()

			// Make a copy to "snapshot" current metrics state.
			metrics <- stats
		}
	}()
	return metrics
}
