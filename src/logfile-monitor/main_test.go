package main

import (
	"bytes"
	"context"
	"fmt"
	"logfile-monitor/config"
	"logfile-monitor/pipeline"
	"logfile-monitor/test"
	"math/rand"
	"runtime"
	"strings"
	"testing"
	"time"
)

// High-level functional tests of the pipeline.
func TestPipeline(t *testing.T) {
	t.Run("Source-Metrics", func(t *testing.T) {
		logfile, err := test.NewLogFileGenerator()
		if err != nil {
			t.Error(err)
		}
		if err := logfile.Create(); err != nil {
			t.Error(err)
		}
		t.Log("log file is", logfile.File.Name())

		ctx, cancel := context.WithCancel(context.Background())
		var output bytes.Buffer
		displayTime := time.Second

		logs := pipeline.SourceStage(ctx, logfile.File.Name())
		metrics := pipeline.MetricsStage(ctx, logs, &output, displayTime)

		// For each metrics, check values.
		start := time.Now()
		now := start
		logfile.GenerateNewRecord(now)
		<-metrics
		for i := 1; i < 10; i++ {
			// Generate a new log with given fake current time
			now = now.Add(time.Duration(1+rand.Intn(2)) * time.Second)
			logfile.GenerateNewRecord(now)

			// Get the metrics coming from the pipeline.
			metric := <-metrics

			// Check the metrics
			expectedRequests := uint64(i + 1)
			if metric.Requests() != expectedRequests {
				test.Fail(t, expectedRequests, metric.Requests())
			}
			expectedDuration := now.Sub(start)
			if metric.Duration() != expectedDuration {
				test.Fail(t, expectedDuration, metric.Duration())
			}
			if rps := float64(expectedRequests) / expectedDuration.Seconds(); rps != metric.RequestsPerSecond() {
				test.Fail(t, rps, metric.RequestsPerSecond())
			}
		}

		// Wait for the metrics summary
		time.Sleep(displayTime + time.Millisecond)
		if line, err := output.ReadString('\n'); err != nil {
			test.Fail(t, nil, err)
		} else if expected := "=== Summary Statistics ===\n"; line != expected {
			test.Fail(t, expected, line)
		}

		fmt.Print(output.String())

		// Cancel the context and check the output channel is then closed.
		cancel()
		_, notClosed := <-metrics
		if expected := false; notClosed != expected {
			test.Fail(t, expected, notClosed)
		}
	})

	t.Run("Source-Metrics-Alerts", func(t *testing.T) {
		logfile, err := test.NewLogFileGenerator()
		if err != nil {
			t.Error(err)
		}
		if err := logfile.Create(); err != nil {
			t.Error(err)
		}
		t.Log("log file is", logfile.File.Name())

		ctx, cancel := context.WithCancel(context.Background())
		var output bytes.Buffer

		rpsThreshold := float64(5)
		done := Pipeline(ctx, &output, logfile.File.Name(), rpsThreshold)

		// Enforce the alert state by inserting more logs than the threshold RPS
		count := 10 * rpsThreshold
		start := time.Now()
		now := start
		for i := 0; float64(i) < count; i++ {
			// Generate a new log with given fake current time
			now = now.Add(time.Duration(1+rand.Intn(100)) * time.Millisecond)
			logfile.GenerateNewRecord(now)
		}

		// Check the alert
		runtime.Gosched()
		fmt.Print(output.String())
		var (
			alert float64
			date  string
		)
		_, err = fmt.Fscanf(&output, strings.TrimRight(config.AlertingMessage, "\n"), &alert, &date)
		if err != nil {
			t.Error(err)
		}
		if expected := rpsThreshold; alert < expected {
			test.Fail(t, expected, alert)
		}
		output.ReadString('\n')

		// Check the alert disappears
		now = now.Add(time.Hour)
		for i := 0; float64(i) < count; i++ {
			// Generate a new log with given fake current time
			now = now.Add(time.Duration(1+rand.Intn(100)) * time.Millisecond)
			logfile.GenerateNewRecord(now)
		}
		runtime.Gosched()
		fmt.Print(output.String())
		_, err = fmt.Fscanf(&output, strings.TrimRight(config.AlertingRecoveredMessage, "\n"), &date)
		if err != nil {
			t.Error(err)
		}

		// Cancel the context and check the output channel is then closed.
		cancel()
		_, notClosed := <-done
		if expected := false; notClosed != expected {
			test.Fail(t, expected, notClosed)
		}
	})
}
