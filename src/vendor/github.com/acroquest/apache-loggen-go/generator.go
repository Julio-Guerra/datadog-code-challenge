package loggen

import (
	"fmt"
	"math"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"time"
)

var (
	ipList []string
)

func causeErr(errRate float64) bool {
	rand.Seed(time.Now().UnixNano())
	for i := 1.0; ; i *= 10 {
		if errRate*i >= 1.0 {
			n := rand.Intn(100 * int(i))
			if int(errRate*i) > n {
				return true
			} else {
				return false
			}
		}
	}
}

func floatToIntString(input float64) string {
	return strconv.Itoa(int(input))
}

func increnemt(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func intToString(input int) string {
	return strconv.Itoa(input)
}

func outputRecord(tick time.Time, config *Config) (record string) {
	filename := config.File.Name()
	if filename == "" {
		record = GetRecord(tick, config)
		fmt.Println(record)
	} else {
		record = GetRecord(tick, config)
		config.File.Write(([]byte)(record + "\n"))
		config.File.Sync()
	}
	return
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

// Generate random number based on log-normal distribution
func randLogNormal(mu, sigma float64) float64 {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1).Float64()
	r2 := rand.New(s1).Float64()
	z := mu + sigma*math.Sqrt(-2.0*math.Log(r1))*math.Sin(2.0*math.Pi*r2)
	return math.Exp(z)
}

func zitter(i int) int {
	rand.Seed(time.Now().UnixNano())
	min := i - randInt(1, 2)
	max := i + randInt(1, 2)
	if min < 0 {
		min = 1
	}
	return min + rand.Intn(max-min)
}

func GenerateNewRecord(config *Config, t time.Time) (record string) {
	return outputRecord(t, config)
}

func GetRecord(t time.Time, config *Config) string {
	if config.Format == "" {
		return Ipv4Address(config.Prefix) + " - - [" + RequestTime(t) + "] \"" + Request(config) + "\" " + HttpStatusCode(config.ErrRate) + " " + SizeofBytes(config) + " " + "\"" + Referer() + "\" " + " \"" + UserAgent(config) + "\" " + ResponseTime(config)
	} else {
		return parseFormat(t, config)
	}
}

func parseFormat(t time.Time, config *Config) string {
	base := config.Format

	formatted := strings.Replace(base, "%h", Ipv4Address(config.Prefix), 1)
	formatted = strings.Replace(formatted, "%l", "-", 1)
	formatted = strings.Replace(formatted, "%u", "-", 1)
	formatted = strings.Replace(formatted, "%t", "["+RequestTime(t)+"]", 1)
	formatted = strings.Replace(formatted, "%r", Request(config), 1)
	formatted = strings.Replace(formatted, "%>s", HttpStatusCode(config.ErrRate), 1)
	formatted = strings.Replace(formatted, "%b", SizeofBytes(config), 1)
	formatted = strings.Replace(formatted, "%{Referer}i", Referer(), 1)
	formatted = strings.Replace(formatted, "%{User-Agent}i", UserAgent(config), 1)
	formatted = strings.Replace(formatted, "%D", ResponseTime(config), 1)

	return formatted
}

func Ipv4Address(cidr string) string {
	if len(ipList) == 0 {
		v4addr, ipnet, err := net.ParseCIDR(cidr)
		if err != nil {
			panic(err)
		}

		for v4addr := v4addr.Mask(ipnet.Mask); ipnet.Contains(v4addr); increnemt(v4addr) {
			ipList = append(ipList, v4addr.String())
		}
	}

	ip := ipList[rand.Intn(len(ipList))]
	return ip
}

func HttpStatusCode(errRate float64) string {
	rand.NewSource(time.Now().UnixNano())
	if causeErr(errRate) == false {
		return "200"
	} else {
		s := []string{"301", "403", "404", "500"}
		return s[rand.Intn(len(s))]
	}
}

func Request(config *Config) string {
	category := config.Categories[rand.Intn(len(config.Categories))]

	i := rand.Intn(10)
	if i < 7 {
		return RequestType() + " /" + category + " HTTP/1.1"
	} else {
		return RequestType() + " /" + category + "/" + intToString(randInt(1, 999)) + " HTTP/1.1"
	}
}

func Referer() string {
	referer := "-"
	return referer
}

func RequestTime(t time.Time) string {
	return t.Format("02/Jan/2006:15:04:05 -0700")
}

func RequestType() string {
	s := []string{"GET", "POST", "PUT", "DELETE"}
	return s[rand.Intn(len(s))]
}

func ResponseTime(config *Config) string {
	return floatToIntString(randLogNormal(config.ResponseTime.Mu, config.ResponseTime.Sigma) * float64(config.ResponseTime.Value))
}

func UserAgent(config *Config) string {
	if len(config.UserAgents) == 0 {
		return "-"
	}
	rand.Seed(time.Now().UTC().UnixNano())
	useragent := config.UserAgents[rand.Intn(len(config.UserAgents))]
	return useragent
}

func SizeofBytes(config *Config) string {
	return floatToIntString(randLogNormal(config.Bytes.Mu, config.Bytes.Sigma) * float64(config.Bytes.Value))
}
