project/name := logfile-monitor
config/logfile/mountpoint := /var/log/access.log

.PHONY: all
all: docker-build

.PHONY: test
test:
	docker build --no-cache --target=test -t $(project/name)-test .

.PHONY: docker-build
docker-build:
	docker build --target=dev -t $(project/name)-dev .

.PHONY: docker-image
docker-image: # dep to docker-build managed by Dockerfile
	docker build --target=prod -t $(project/name) .

.PHONY: run
logfile = $(if $(ARG_LOGFILE),-v $(ARG_LOGFILE):$(config/logfile/mountpoint))
run: docker-image
	docker run --rm $(logfile) $(project/name)

.PHONY: test-validation
.ONESHELL:
test-validation: docker-image
	set -e
	nginx=$$(docker run --rm -d -v /var/log/nginx -p 8080:80 nginx:stable-alpine)
	# Hack to remove the link to /dev/stdout
	docker exec $$nginx rm /var/log/nginx/access.log
	docker restart $$nginx
	trap "docker stop $$nginx" INT TERM
	docker run --rm --volumes-from=$$nginx $(project/name) -logfile=/var/log/nginx/access.log
