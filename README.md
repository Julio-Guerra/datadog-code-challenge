# About

This is my submission to the following Datadog's code challenge assignment. It
contains the Go source code of a monitoring software of common log files, and
everything required to compile and test it.

This README file explains how the submission works, what is inside the
repository, and the main software design decisions.

# Assignment

From Kristina Kysova <kristina.kysova@datadoghq.com> to Julio Guerra
<guerra@julio.in>:

> At Datadog, we value working on real solutions to real problems, and as such we
> think the best way to understand your capabilities is to give you the
> opportunity to solve a problem similar to the ones we solve on a daily basis. As
> the next step in our process, we ask that you write a simple console program
> that monitors HTTP traffic on your machine. Treat this as an opportunity to show
> us how you would write something you would be proud to put your name on. Feel
> free to impress us.
> 
> Consume an actively written-to w3c-formatted HTTP access log
> (https://en.wikipedia.org/wiki/Common_Log_Format). It should default to reading
> /var/log/access.log and be overridable. Example log lines:
> 
> ```
> 127.0.0.1 - james [09/May/2018:16:00:39 +0000] "GET /report HTTP/1.0" 200 1234
> 127.0.0.1 - jill [09/May/2018:16:00:41 +0000] "GET /api/user HTTP/1.0" 200 1234
> 127.0.0.1 - frank [09/May/2018:16:00:42 +0000] "GET /api/user HTTP/1.0" 200 1234
> 127.0.0.1 - mary [09/May/2018:16:00:42 +0000] "GET /api/user HTTP/1.0" 200 1234
> ```
> 
> Display stats every 10s about the traffic during those 10s: the sections of the
> web site with the most hits, as well as interesting summary statistics on the
> traffic as a whole. A section is defined as being what's before the second '/'
> in the path. For example, the section for "http://my.site.com/pages/create” is
> "http://my.site.com/pages". Make sure a user can keep the app running and
> monitor the log file continuously Whenever total traffic for the past 2 minutes
> exceeds a certain number on average, add a message saying that “High traffic
> generated an alert - hits = {value}, triggered at {time}”. The default threshold
> should be 10 requests per second and should be overridable. Whenever the total
> traffic drops again below that value on average for the past 2 minutes, print or
> displays another message detailing when the alert recovered. Write a test for
> the alerting logic. Explain how you’d improve on this application design. If you
> have access to a linux docker environment, we'd love to be able to docker build
> and run your project! If you don't though, don't sweat it. As an example:

# Usage

## Command-line interface

```console
$ logfile-monitor -help
Usage of logfile-monitor:
  -alert-rps-threshold float
        Request per second threshold of the alerting system. (default 10)
  -logfile string
        Log file to monitor. (default "/var/log/access.log")
```

## Docker image

Even if everything required to compile the software is explained below, its
docker image is also available online at
https://hub.docker.com/r/julio/logfile-monitor/

```console
# docker run --rm julio/logfile-monitor -help
Usage of /usr/local/bin/logfile-monitor:
  -alert-rps-threshold float
        Request per second threshold of the alerting system. (default 10)
  -logfile string
        Log file to monitor. (default "/var/log/access.log")
```

For example, to use it with another Nginx container, do:

```
nginx=$(docker run --rm -d -v /var/log/nginx -p 8080:80 nginx:stable-alpine)
# Workaround to remove the link to /dev/stdout
docker exec $$nginx rm /var/log/nginx/access.log
docker restart $$nginx
docker run --rm --volumes-from=$nginx julio/logfile-monitor -logfile=/var/log/nginx/access.log
```

This is the method used in the Makefile to perform the validation test.

# Content

```
.
├── Dockerfile
├── Makefile
├── README.md
├── .gitlab-ci.yml
└── src
    ├── logfile-monitor
    │   ├── config
    │   ├── main.go
    │   ├── main_test.go
    │   ├── metrics
    │   ├── parser
    │   ├── pipeline
    │   ├── source
    │   └── test
    └── vendor
```

The repository is pretty straighforward and includes only strictly necessary
files:

- Makefile: set of commands using Docker and the Dockerfile.
- Dockerfile: packages into containers the development, testing and production
  environments, using docker's multi-stage builds.
- .gitlab-ci.yml: CI pipeline for GitLab.
- src/: source files.
  - vendor/: external dependencies statically included in the repository for the
    sake of simplicity.
  - logfile-monitor/: the software's source code.

# Development

## Requirements

Everything is "containerized" and simply executed from a Makefile. Which
restricts the required tools to:

- GNU Make
- Docker

## Continuous integration pipeline

File `.gitlab-ci.yml` defines a simple GitLab CI pipeline with two stages, one
for the build, another for the tests. To reuse it, simply push it to your GitLab
repository and it should automatically detect the file and use it. It uses a
custom docker image including GNU Make.

## Compilation

Run:

```console
$ make
docker build --target=dev -t logfile-monitor-dev .
...
```

It uses the Dockerfile and builds its build stage, which includes every required
Go compilation tools.

## Unit and functional tests

Run:

```console
$ make test
docker build --no-cache --target=test -t logfile-monitor-test .
...
```

It uses the Dockerfile and builds its test stage, which includes every required
Go testing tools. It runs the Go test files `*_test.go` included in the source
tree.

## Docker image

Run:

```console
$ make docker-image
docker build --target=prod -t logfile-monitor .
...
```

It uses the Dockerfile and builds its production stage, which copies the binary
from the build stage.

## Validation test

To perform an overall validation test, based on the final binary program, the
following command runs the docker image, along with an nginx server. It is then
possible to test the program using a browser or tools like `siege`.

Run:

```console
$ make test-validation
docker build --target=prod -t logfile-monitor .
...
nginx=$(docker run --rm -d -v /var/log/nginx -p 8080:80 nginx:stable-alpine)
...
docker run --rm --volumes-from=$nginx logfile-monitor -logfile=/var/log/nginx/access.log
...
```

### Using a browser

Open the local TCP port 8080 to access the default nginx's virtual host, which
generates access logs, monitored by the program.

### Using siege

`siege` allows to perform validation tests. For example, the following command
will concurrently access `/` 50 times every 100 ms:

```console
$ siege -c4 -r 50 -d 0.1 127.0.0.1:8080/
```

## Execute the docker image

To execute the docker image to monitor a log file in your host system, such as
`/var/log/access.log`, an helper command is provided in the Makefile:

```console
$ make run ARG_LOGFILE=/var/log/access.log
...
docker run --rm -v /var/log/daemon.log:/var/log/access.log logfile-monitor
...
```

# Limitations

The program only reacts to new logs. Which means that metrics and alerts will be
updated for each new log only:
- displayed summary stats will show metrics based on logged activity only.
- alerts are updated when new logs are received. Which means that an alert won't
  disappear if no more activity is logged, as the requests per seconds will be
  updated when new logs are received.

# Design Decisions

## Development environment

The development environment was designed to be simple, only with the necessary
tools and using them the simple way:

- Docker is used to build the program but the simple way, which is different
  from the way I in fact prefer using it for big projects: I rather mount the
  source tree as a container volume instead of copying the source tree inside a
  container. It allows faster compilation time (files are no longer copied) and
  outputs everything in-place.  

- The Makefile calls Docker but the simple way: I rather recursively call the
  Makefile (e.g. make target => docker exec … make target => container
  toolchain) to make the Makefile the single source of truth and especially to
  make benefit of its dependencies management and parallel compilation features.
  I avoided it here as it requires [extra Makefile
  hacks](https://github.com/farjump/Makefile.in/blob/dev/src/mk/toolchain/docker.mk).

- External dependencies have been copied into the source tree to avoid using
  vendoring tools and thus keep the repository simple. I otherwise rather use go
  modules (go v1.11) or any go vending tool to avoid copying them in the source
  tree without any version reference.

- I used the bare go test framework which mainly lacks from assertion libraries.

## Implementation

The source code implements a pipeline of three parallel stages, communicating
with queues (Go channels):
- stage 1 - file reader and parser: "tail" the file, validate each new log
  entry, and queue them for the next stage.
- stage 2 - metrics: update the metrics based on dequeued log entries and queue
  them for the next stage.
- stage 3 - alerts: dequeue metrics, check the values and alert when necessary.

A separate goroutine in stage 2 displays the stats summary every 10 seconds.

```
+------+        +-------+           +------+
|Source|--Log-->|Metrics|--Metric-->|Alerts|
+------+        +-------+           +------+
```

Everything happens in parallel and queues' lengths are statically configurable
in `src/logfile-monitor/config/config.go`. The general rule for such pipeline is
to avoid starvation of stages and make sure they can always be running, i.e.
always have incoming data to process. The other important rule is to avoid
short-running stages, which would be slower than a simple function call, because
of context-switch durations (even for coroutines).

### Implementation comments:

- Coroutines and queues are built into Go, and it’s kind of easy and elegant to
  write such code, but using goroutines and channels is definitely heavyweight
  for very small computations, which would be more efficient with a simple
  event-based implementation (e.g. `OnNewLogEntry(handler)`). This is something
  that should be benchmarked to compare the different approaches based on the
  data to process.

- Using buffered channels between goroutines allows to make the pipeline stages
  asynchronous and maximise their parallelism, as they won't block when sending
  or receiving data, unless there is no room or data in the channel.

- The format of each new log entry is validated in the first stage instead of a
  separate one, as it runs a regular expresion and is very fast once compiled.
  It is thus not worth switching to another goroutine.
  
- Log entries are fastly validated using a pre-compiled regular expression and
  then wrapped in a lazy parser, only actually parsing the fields when required.
  Again, the idea here is to feed the next stage as fast as possible and this
  avoids unnecessary work.

- Short implementation of time series, only used by the alerting stage to get
  the last 2 minutes of data. The implementation only fulfill this requirement.
  
- The program reacts to new logs only, and this is when it updates its states.
  It would be better also reacting to some time series event to automatically
  update itself even when there are no new logs.

- Program logs are implemented using Go’s standard package `log` which doesn’t
  offer multiple levels of log levels as generally preferable.

- Use of the standard flag package for the sake of simplicity (no need for an
  external library), but I rather use libraries implementing POSIX-style program
  options and arguments.

- The validation tests are manual here, because the functional tests were easier
  to perform thanks to the possibility to provide fake dates, and thus produce
  fake requests per seconds. High-level functional tests of the pipeline are in
  file `src/logfile-monitor/main_test.go`. A true project would require using a
  good validation test framework to easily communicate and synchronize with a
  running program.

- Note that I use logging libraries such as Go package `log` only as general
  fast helping debugging mean, and of course doesn’t replace a good **run time**
  debugger such as GDB :)

- I tried to avoid over-engineering, so there are no abstract interfaces if they
  are not necessary. They would have been in case of many sources of logs, log
  formats, etc.

- Modularity is far from perfect as it was written fastly. The source code can
  be further factorized and enhanced.

- Git logs are short as they ware written fastly. I rather try to exhaustively
  explain what the patch does.
