FROM golang:1.10 AS dev-base
COPY src/. /go/src/
WORKDIR /go/src/logfile-monitor

FROM dev-base AS dev
RUN go install -v ./...

FROM dev-base AS test
RUN go test -v -timeout 30s ./...

FROM alpine:latest AS prod
WORKDIR /
COPY --from=dev /go/bin/logfile-monitor /usr/local/bin/
ENTRYPOINT [ "/usr/local/bin/logfile-monitor" ] 